### Prepare butor-plugins
rm -r ./node_modules/jsdoc/templates/butor
cp -R ./butor_template ./node_modules/jsdoc/templates/
mv ./node_modules/jsdoc/templates/butor_template ./node_modules/jsdoc/templates/butor

### Prepare butor-template
cp ./butor_plugins/* ./node_modules/jsdoc/plugins/

### Make JSDoc
./node_modules/.bin/jsdoc -c ./conf.json
