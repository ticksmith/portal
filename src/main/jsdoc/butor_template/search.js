var docletChecker = {  };
var docletCheckersByKind = {};

numOfDoclet = 0;

data().each(function(doclet) {
	var kinds = Object.keys(docletCheckersByKind);
	if (kinds.indexOf(doclet.kind) == -1)
		docletCheckersByKind[doclet.kind] = {
			'numOfDoclet': 0,
			'properties': {},
			'functions': {}
		};

	var docletChecker = docletCheckersByKind[doclet.kind];
	docletChecker.numOfDoclet += 1;

	for (property in doclet) {
		if (doclet[property]) {
			if (typeof doclet[property] != 'function') {
				if (docletChecker.properties[property]) {
					docletChecker.properties[property] += 1;
				}
				else {
					docletChecker.properties[property] = 1;
				}
			}
			else {
				if (docletChecker.functions[property]) {
					docletChecker.functions[property] += 1;
				}
				else if (doclet[property]) {
					docletChecker.functions[property] = 1;
				}
			}
		}
	}
});

data().each(function(doclet) {
	excludedProperties = [
		'kind',
		'longname',
		'access',
		'see',
		'todo',
		'id',
		'__s',
		'postProcess',
		'addTag',
		'setMemberof',
		'setLongname',
		'setScope',
		'borrow',
		'mix',
		'augment',
		'setmMeta',
		'meta',
		'description',
		'name',
		'scope',
		'comment',
		'memberof',
		'see',
		'example',
		'requires',
		'params',
		'fires',
		'virtual',
		'shortName',
		'returns',
		'properties',
		'files',
		'shortName',
		'type',
		'copyright',
		'licence',
		'preserveName',
		'defaultvalue',
		'shortname',
	];

	var kinds = Object.keys(docletCheckersByKind);
	if (kinds.indexOf(doclet.kind) == -1)
		docletCheckersByKind[doclet.kind] = {
			'numOfDoclet': 0,
			'properties': {},
			'functions': {}
		};

	var docletChecker = docletCheckersByKind[doclet.kind];
	docletChecker.numOfDoclet += 1;

	for (property in doclet) {
		if (excludedProperties.indexOf(property) != -1)
			return;

		if (doclet[property]) {
			if (typeof doclet[property] != 'function') {
				if (docletChecker.properties[property]) {
					docletChecker.properties[property] += 1;
				}
				else {
					docletChecker.properties[property] = 1;
				}
			}
			else {
				if (docletChecker.functions[property]) {
					docletChecker.functions[property] += 1;
				}
				else if (doclet[property]) {
					docletChecker.functions[property] = 1;
				}
			}
		}
	}
});
