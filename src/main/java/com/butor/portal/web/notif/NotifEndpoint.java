/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.portal.web.notif;

import java.io.EOFException;
import java.io.IOException;

import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

public class NotifEndpoint extends Endpoint {
	private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        RemoteEndpoint.Basic remoteEndpointBasic = session.getBasicRemote();
        
        if (NotifContextListener.getNotifManager() == null) {
        	logger.warn("Notification are not enabled! bye");
        	try {
				session.close();
			} catch (IOException e) {
				logger.warn("Failed closing session", e);
			}
        	return;
        }
        
        NotifSession ns = new NotifSession(session.getId(), 
        	session.getUserPrincipal().getName(), 
        	remoteEndpointBasic);

        session.addMessageHandler(ns);

        NotifContextListener.getNotifManager().addSession(ns);
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
    	NotifContextListener.getNotifManager().removeSession(session.getId());
    }

    @Override
    public void onError(Session session, Throwable thr) {
        Throwable root = Throwables.getRootCause(thr);
        if (root instanceof EOFException) {
            // connection closed by client
        } else {
            logger.error("onError: " +thr.toString(), thr);
        }
    }
}
