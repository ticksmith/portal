/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.portal.web.ajax;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.butor.attrset.common.AttrSet;
import org.butor.auth.common.AuthServices;
import org.butor.auth.common.func.Func;
import org.butor.json.JsonHelper;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.json.util.ContextBuilder;
import org.butor.utils.CommonDateFormat;
import org.butor.utils.Message;
import org.butor.utils.Message.MessageType;
import org.butor.web.servlet.AjaxContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.butor.portal.common.userProfile.UserProfileServices;
import com.google.common.base.Throwables;

public class PortalAjaxCmp implements InitializingBean {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private AuthServices authServices;
	private UserProfileServices profileServices;

	private String menuDefJson;
	private String appsDefJson;
	private String pagesDefJson;

	private Object menuDef;
	private Object appsDef;
	private Object pagesDef;

	public PortalAjaxCmp() {
		super();
	}

	public void getSetup(final Context ctx) {
		final ResponseHandler resp = ctx.getResponseHandler();
		AjaxContext ac = (AjaxContext) ctx;

		try {
			final List<Func> funcs = new ArrayList<Func>();
			final ResponseHandler<Func> handler = new ResponseHandler<Func>() {
				public boolean addMessage(Message msg_) {
					//resp.addMessage(msg_);
					return true;
				}
				public boolean addRow(Func row) {
					funcs.add(row);
					return true;
				}
				public void end() {
					//OK
				}
				@Override
				public Type getResponseType() {
					return Func.class;
				}
			};
			String username = null;
			if (ac.getHttpServletRequest().getUserPrincipal() != null)
				username = ac.getHttpServletRequest().getUserPrincipal().getName();

			ac.getRequest().setUserId(username);
			Context<Func> sctx = new ContextBuilder<Func>()
					.setCommonRequestArgs(ac.getRequest())
					.setResponseHandler(handler).build();

			Map<String, Object> result = new HashMap<String, Object>();
			authServices.listAuthFunc(sctx);
			result.put("funcs", funcs);
			result.put("menuDef", menuDef);
			result.put("appsDef", appsDef);
			result.put("pagesDef", pagesDef);

			resp.addRow(result);

		} catch (Exception ex) {
			resp.addMessage(new Message(0, MessageType.ERROR, Throwables.getRootCause(ex).toString()));
		}
	}
	public void getProfile(final Context ctx) {
		final ResponseHandler resp = ctx.getResponseHandler();
		AjaxContext ac = (AjaxContext) ctx;

		try {
			final Map<String, String> profile = new HashMap<String, String>();
			ResponseHandler<AttrSet> handler = new ResponseHandler<AttrSet>() {
				public boolean addMessage(Message msg_) {
					resp.addMessage(msg_);
					return true;
				}
				public boolean addRow(AttrSet row) {
					profile.put(row.getK1(), row.getValue());
					return true;
				}
				public void end() {
					//OK
				}
				@Override
				public Type getResponseType() {
					return AttrSet.class;
				}
			};
			String username = null;
			if (ac.getHttpServletRequest().getUserPrincipal() != null)
				username = ac.getHttpServletRequest().getUserPrincipal().getName();

			ac.getRequest().setUserId(username);
			Context<AttrSet> sctx = new ContextBuilder<AttrSet>()
					.setCommonRequestArgs(ac.getRequest())
					.setResponseHandler(handler).build();

			profileServices.readProfile(sctx);
			resp.addRow(profile);

		} catch (Exception ex) {
			resp.addMessage(new Message(0, MessageType.ERROR, Throwables.getRootCause(ex).toString()));
		}
	}
	public void updateProfileAttr(final Context ctx, String key, String val) {
		final ResponseHandler resp = ctx.getResponseHandler();
		AjaxContext ac = (AjaxContext) ctx;

		try {
			final Map<String, String> entry = new HashMap<String, String>();
			ResponseHandler<AttrSet> handler = new ResponseHandler<AttrSet>() {
				public boolean addMessage(Message msg_) {
					resp.addMessage(msg_);
					return true;
				}
				public boolean addRow(AttrSet row) {
					entry.put(row.getK1(), row.getValue());
					return true;
				}
				public void end() {
				}
				@Override
				public Type getResponseType() {
					return AttrSet.class;
				}
			};
			String username = null;
			if (ac.getHttpServletRequest().getUserPrincipal() != null)
				username = ac.getHttpServletRequest().getUserPrincipal().getName();

			ac.getRequest().setUserId(username);
			Context<AttrSet> sctx = new ContextBuilder<AttrSet>()
					.setCommonRequestArgs(ac.getRequest())
					.setResponseHandler(handler).build();

			AttrSet attr = new AttrSet();
			attr.setType("user");
			attr.setId(username);
			attr.setK1(key);
			attr.setK2(".");//TODO
			attr.setValue(val);
			profileServices.updateAttr(sctx, attr);
			resp.addRow(entry);

		} catch (Exception ex) {
			resp.addMessage(new Message(0, MessageType.ERROR, Throwables.getRootCause(ex).toString()));
		}
	}
	public void deleteProfileAttr(final Context ctx, String key) {
		final ResponseHandler resp = ctx.getResponseHandler();
		AjaxContext ac = (AjaxContext) ctx;

		try {
			ResponseHandler<AttrSet> handler = new ResponseHandler<AttrSet>() {
				public boolean addMessage(Message msg_) {
					resp.addMessage(msg_);
					return true;
				}
				public boolean addRow(AttrSet row) {
					return true;
				}
				public void end() {
					//OK
				}
				@Override
				public Type getResponseType() {
					return AttrSet.class;
				}
			};
			String username = null;
			if (ac.getHttpServletRequest().getUserPrincipal() != null)
				username = ac.getHttpServletRequest().getUserPrincipal().getName();

			ac.getRequest().setUserId(username);
			Context<AttrSet> sctx = new ContextBuilder<AttrSet>()
					.setCommonRequestArgs(ac.getRequest())
					.setResponseHandler(handler).build();

			AttrSet attr = new AttrSet();
			attr.setType("user");
			attr.setId(username);
			attr.setK1(key);
			attr.setK2(".");//TODO
			profileServices.deleteAttr(sctx, attr);

		} catch (Exception ex) {
			resp.addMessage(new Message(0, MessageType.ERROR, Throwables.getRootCause(ex).toString()));
		}
	}

	public void ping(final Context ctx) {
		ResponseHandler resp = ctx.getResponseHandler();
		resp.addRow(CommonDateFormat.YYYYMMDD_HHMMSS_WITHMS.format(new Date()));
	}

	@Override
	public void afterPropertiesSet() throws Exception {
/*A.S.		logger.info("authNamespace={}", authNamespace);
		logger.info("authUrl={}", authUrl);
		logger.info("profileNamespace={}", profileNamespace);
		logger.info("profileUrl={}", profileUrl);

		authSCF = new ServiceCallerFactory(authNamespace, authUrl);
		authServices = authSCF.createServiceCaller(AuthServices.class);
		profileSCF = new ServiceCallerFactory(profileNamespace, profileUrl);
		profileServices = profileSCF.createServiceCaller(UserProfileServices.class);
*/
		logger.info("menuDefJson={}", menuDefJson);
		logger.info("appsDefJson={}", appsDefJson);
		logger.info("pagesDefJson={}", pagesDefJson);
		JsonHelper jsh = new JsonHelper();
		logger.info("Deserializing apps definitions JSON ...");
		appsDef = jsh.deserialize(appsDefJson, Object.class);

		logger.info("Deserializing pages definitions JSON ...");
		pagesDef = jsh.deserialize(pagesDefJson, Object.class);

		logger.info("Deserializing menu definition JSON ...");
		menuDef = jsh.deserialize(menuDefJson, Object.class);
	}

	public void setMenuDefJson(String menuDefJson) {
		this.menuDefJson = menuDefJson;
	}

	public void setAppsDefJson(String appsDefJson) {
		this.appsDefJson = appsDefJson;
	}

	public void setPagesDefJson(String pagesDefJson) {
		this.pagesDefJson = pagesDefJson;
	}

	public void setAuthServices(AuthServices authServices) {
		this.authServices = authServices;
	}

	public void setProfileServices(UserProfileServices profileServices) {
		this.profileServices = profileServices;
	}
}
