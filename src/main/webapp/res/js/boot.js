/*
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
$(document).ready(function() {
	$.ajaxSetup({cache : false});
	(function() {
		var devMode = false;
		var reload = function() {
			$('body').prepend('Loading ...');
			setTimeout(function() {
				//document.location="/";
			}, 7000);
		}
		var loadCSS = function(url) {
			var ref = document.createElement("link");
			ref.rel = "stylesheet";
			ref.type = "text/css";
			ref.href = url +(url.indexOf("?") == -1 ? "?_" : "&_") +Math.random();
			document.getElementsByTagName("head")[0].appendChild(ref);
		};

		loadCSS("res/butor/css/style.css");
		loadCSS("res/css/style.css");
		loadCSS("/wl?c=1");
		loadCSS("res/butor/css/butor-table.css");

		$.getScript(devMode ? 'res/butor/js/butor.js' : 'res/butor/js/butor.min.js')
		.done(function() {
			LOGGER.setLevel(LOGGER.INFO);
			// expose google analytics push globally
			window.gaPush = butor.ga.push;

			var loader = new butor.Loader();
			loader.bind('loadingScript', function (e_) {
				var data = e_.data;
				LOGGER.info(JSON.stringify(e_));
				if (data.loadName === 'boot') {
					App.mask('Loading ' +parseInt((data.index/data.total)*100) +'% ...');
				}
			});
			loader.bind('scriptLoaded', function (e_) {
				var data = e_.data;
				LOGGER.info(JSON.stringify(e_));
				if (data.loadName === 'boot') {
					if (data.index  === data.total) {
						if (devMode) {
							$('<div class="text-center"><h1 class="text-danger">running in debug mode!</h1></div>').appendTo('body');
						}

						App.unmask();
					}
					if (data.success === false) {
						reload();
					}
				}
			});
			var modules = ["/sso/res/js/ticket.js"];

			if (devMode) {
				modules = modules.concat([
					"assets-all.js",
		  			"res/butor/js/butor-app.js",
					"res/butor/js/butor-dlg.js",
					"res/butor/js/bundle.js",
					"res/butor/js/butor-table.js",
					"res/butor/js/butor-row-editor.js",
					"res/butor/js/butor-panels.js",
					"res/butor/js/butor-live-search.js",
					"res/butor/js/butor-upload.js"]);
			} else {
				modules = modules.concat(["assets-all.min.js"])
			}
			
			loader.loadScripts(modules, 'boot');
		})
		.fail(function(jqxhr, settings, exception) {
			reload();
		});
	}());
});

//# sourceURL=butor.portal.boot.js
