java -jar ${CLOSURE_HOME}/compiler.jar \
--js ../../main/webapp/res/js/portal.js \
--js_output_file portal.min.js \
--language_in ECMASCRIPT5 \
--compilation_level SIMPLE_OPTIMIZATIONS \
--externs extern.js 
#\
#--externs login-extern.js

cp portal.min.js ../../main/webapp/res/js/
